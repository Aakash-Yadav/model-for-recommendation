from setuptools import setup
from Cython.Build import cythonize
import numpy

setup(
    ext_modules=cythonize("anime_names.pyx"),
    include_dirs=[numpy.get_include()]
)

from os import system,listdir


for i in listdir('.'):
    if i == 'anime_names.c' or i== 'build':
        system('rm -rf %s'%(i))

