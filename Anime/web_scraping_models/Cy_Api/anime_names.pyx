
import lxml.html


cpdef get_the_anime_names(str html_code):
    
    data = lxml.html.fromstring(html_code);
    
    cdef list all_li_tags = data.xpath("//ul[@class='items']/li/a");

    cdef unsigned int i;

    cdef list  results = [0 for i in range(len(all_li_tags))];
    
    for i in range(len(all_li_tags)):
        results[i] = (all_li_tags[i].text_content().strip().lower(),all_li_tags[i].get('href').strip()[7:-5])

    return results;



