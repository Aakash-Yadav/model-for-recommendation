from json import loads,dumps
from requests import Session
from string import ascii_uppercase
from concurrent.futures import ThreadPoolExecutor
"Local import"
from encryption.encryption_model import convert_to_decrypt
"Cython import"
from Cy_Api import anime_names


def return_api_txt(n: int):
    with open('Json/API.json', 'r') as F:
        data = loads(F.read())
    url_key = data[list(data.keys())[n-1]][2:].encode()
    return convert_to_decrypt(url_key)

def sort_data(n:dict):
  x=sorted(n.items(),key=lambda t: t[0])
  return dict(x)

class Fast_Boii(object):

    __slots__ = ("function","value");

    def __init__(self,function,value):
        self.function = function;
        self.value = value;

    def thread_pool_executor(self):
        with ThreadPoolExecutor() as Texe:
            data = Texe.map(self.function,self.value);
        return data;

    def __repr__(self):
        return f"function = {self.function.__name__}, vlaue = {self.value}";

###################
### MAin class ###
##################

FEATCH = Session().get

class Anime_data_saver():

    __slots__ = ("api","all_char","html_code");

    def __init__(self):
        self.api = return_api_txt(1);
        self.all_char = list(ascii_uppercase)+["special"];

    def get_the_page_html(self,key:int):
        print(self.all_char[key])
        return anime_names.get_the_anime_names(FEATCH("%s%s"%(self.api,self.all_char[key])).text);

    def get_all_anime_names(self):
        self.html_code = Fast_Boii(self.get_the_page_html,[x for x in range(0,27)]).thread_pool_executor()
        return ((sorted(sum([i for i in self.html_code],[]))))

    def save_file(self,name):

        with open("Json/%s%s"%(name,".json"),"w") as f:
            f.write(f'{dumps(sort_data(dict(self.get_all_anime_names())) , indent=2)}')

if __name__ == "__main__":
    Anime_data_saver().save_file("names")
