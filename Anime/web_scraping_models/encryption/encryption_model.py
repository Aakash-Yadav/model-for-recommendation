
from cryptography.fernet import Fernet

'''
def write_key():
    key = Fernet.generate_key() # Generates the key
    with open("key.key", "wb") as key_file: # Opens the file the key is to be written to
        key_file.write(key)

write_key()
'''
x= open('encryption/key.key','rb')

data = x.read()

x.close()

f = Fernet(data)

def convert_into_encrypt(n:str):
    n = n.encode()
    return f.encrypt(n)

def convert_to_decrypt(n):
    value = (f.decrypt(n))
    return ''.join(map(chr, value))
