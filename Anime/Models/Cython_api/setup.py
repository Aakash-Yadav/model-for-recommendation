from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules=cythonize("main.pyx"),
)

from os import system,listdir


for i in listdir('.'):
    if i == 'main.c' or i== 'build':
        system('rm -rf %s'%(i))

