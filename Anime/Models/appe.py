
# from tkinter import image_names
from flask import Flask,render_template
from anime_images import img_links
from Cython_api.main import  anime_recomend

app = Flask(__name__);

@app.route('/home')
def home():
    # Ace of the Diamond
    # One Piece
    # Pokemon
    name = "Death Parade".lower();
    predict_data = anime_recomend(name);
    # print(predict_data);
    out = []
    for i,j in predict_data.items():
        out.append(
            (i,img_links.get(i))
            );
    return render_template("home.html",dd=out,name=name);

if __name__ == "__main__":
    app.run(debug=1);