from json import loads, dumps
# from unicodedata import name
from Cython_api import main
from collections import ChainMap


class Data_provider(object):

    __slots__ = ('name')

    def __init__(self, name) -> None:
        self.name = name

    def get_the_anime_future_data(self):
        out_dict = []
        anime_data = dict(main.get_the_anime_year(self.name.lower()))
        anime_data = dict(sorted(anime_data.items(), key=lambda item: item[1]))
        names = list(anime_data)
        print(names)
        # print(anime_data.keys())

        if len(names) <= 50: 
            out_dict.append(anime_data)
            if len(names)<=10:
                mid = 0;
                print("Less 10");
            elif len(names)>10:
                print("more 10")  
                mid = len(names)-10;
            for i in names[mid:]:
                print(i)
                try:
                    anime_d_2 = dict(main.get_the_anime_year(i))
                    anime_data_2 = dict(
                        sorted(anime_d_2.items(), key=lambda item: item[1]))
                    out_dict.append(anime_data_2)
                except:
                    print("Not found")
        
            c = dict(ChainMap(*[x for x in out_dict]))
            names  = list(dict(sorted(c.items(), key=lambda item: item[1])))
            names = names[len(names)//2:];
            
            with open("f.json",'w') as f:
                f.write(f"{dumps({key: c[key] for key in names },indent=2)}")
            return {key: c[key] for key in names }
        else:
            names  = list(dict(sorted(anime_data.items(), key=lambda item: item[1])))
            names = names[len(names)//2:];
            with open("f.json",'w') as f:
                f.write(f"{dumps({key: anime_data[key] for key in names },indent=2)}")
            return {key: anime_data[key] for key in names }

Data_provider("hunter x hunter").get_the_anime_future_data()
