
![alt text](https://xp.io/storage/bwrw5Os.png)

# Content based recommendation model

Content-based filtering is one popular technique of recommendation or recommender systems. The content or attributes of the things you like are referred to as "content."

_Here, the system uses your features and likes in order to recommend you with things that you might like. It uses the information provided by you over the internet and the ones they are able to gather and then they curate recommendations according to that._  

_The goal behind content-based filtering is to classify products with specific keywords, learn what the customer likes, look up those terms in the database, and then recommend similar things._

_This type of recommender system is hugely dependent on the inputs provided by users, some common examples included Google, Wikipedia, etc. For example, when a user searches for a group of keywords, then Google displays all the items consisting of those keywords. The below video explains how a content-based recommender works._

## Classification method :
The second method is the classification method. In it, we can create a decision tree and find out if the user wants to read a book or not.

For example, a book is considered, let it be The Alchemist.

_Based on the user data, we first look at the author name and it is not Agatha Christie. Then, the genre is not a crime thriller, nor is it the type of book you ever reviewed. With these classifications, we conclude that this book shouldn’t be recommended to you._

## Anime?
Anime is a Japanese term for animation. Outside of Japan and in English, anime refers specifically to animation produced in Japan. However, in Japan and in Japanese, anime describes all animated works, regardless of style or origin.

## My Goals :)
The goal is simple: help people find new anime. Whether you're a veteran or completely new to the space, my dream is to ensure that you can find something new and exciting to watch.

## Model Prediction:

#### Jujutsu Kaisen
![alt text](https://user-content.gitlab-static.net/25df5c47a3f19d3fd65aada9f0a2e7798d89ed66/68747470733a2f2f78702e696f2f73746f726167652f624479374e44512e706e67)

#### Death note
![alt text](https://user-content.gitlab-static.net/e9c5b43a21d33df37bf6d982564130a0c93b7d79/68747470733a2f2f78702e696f2f73746f726167652f624530643465432e706e67)

#### Hunter x Hunter
![alt text](https://user-content.gitlab-static.net/3b8afaf781398e6dc9677cee66218cb3fdee7459/68747470733a2f2f78702e696f2f73746f726167652f6244484e324e4a2e706e67)

#### Tokyo ghoul
![alt text](https://user-content.gitlab-static.net/41a417ea750963d0490371f1686e8fd43a5d3fa6/68747470733a2f2f78702e696f2f73746f726167652f62444c4f316e532e706e67)

#### Platinum end 
![alt text](https://user-content.gitlab-static.net/561f69a861e9de3ed356a0c12c15a30af851175c/68747470733a2f2f78702e696f2f73746f726167652f624451794f484c2e706e67)

#### Doraemon Movie 31: Shin Nobita to Tetsujin Heidan.....
![alt text](https://user-content.gitlab-static.net/d6038856a1b3148fb37feb2f6ed5abbe825d45f4/68747470733a2f2f78702e696f2f73746f726167652f624a6662446f562e706e67)

### Dragon ball
![alt text](https://user-content.gitlab-static.net/f133036005a9769f03fee5d78e8cd6d0b62830ba/68747470733a2f2f78702e696f2f73746f726167652f624a564a6252462e706e67)

# Conclusion
Looking at the methods of content-based recommendation we understood that a computer uses many processes to make our lives easier, one of them is the recommendation process.